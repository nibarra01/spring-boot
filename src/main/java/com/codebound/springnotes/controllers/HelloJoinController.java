package com.codebound.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HelloJoinController {
//    assign a specific url
    @GetMapping("/join")
    public String showJoinForm() {
//        return the name of our view
        return "join-view";
    }

//    set upa post method support for our form
    @PostMapping("/join")
    public String joinClassroom(@RequestParam (name = "cohort") String classroom, Model model){
        model.addAttribute("classroom", "Welcome to " + classroom);
        return "join-view";
    }

//    we set up a method that makes a request to the name attribute in ou rform, turns that in to a parameter
//    for our method as "classroom"being  passes ad a model attribute
//    the model returning the Welcome to classroom to join-view.html where classroom is the value that the user
//    entered in the form
}
