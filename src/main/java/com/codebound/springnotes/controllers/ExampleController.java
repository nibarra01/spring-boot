package com.codebound.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ExampleController {
    @GetMapping("/example")
    @ResponseBody
    public String hello(){
        return "Hello from Spring Boot";
    }
//    first step in building our application we defined a controller and defied what route the controller responds to
}
