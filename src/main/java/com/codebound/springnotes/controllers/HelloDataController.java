package com.codebound.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class HelloDataController {
//    passing data to our viww

//    define a controller method that accepts a model
    @GetMapping("/hello-data/{name}")
    public String helloName(@PathVariable String name, Model model){
        model.addAttribute("name", name);
        return "hello-data-view";
    }
//    Model attributes are used inside controller classes that prepare the data for rendering inside a view
//    One was we can add attributes to our model is to require an instance of Model as a parameter in a controller method
}
