package com.codebound.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class FragmentController {
    @GetMapping("/fragment")
    public String showFragments(){
        return "fragment-view";
    }
}
