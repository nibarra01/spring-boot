package com.codebound.springnotes.controllers;

import com.codebound.springnotes.Models.Book;
import com.codebound.springnotes.repositories.Bookpository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class Booktroller {
    private final Bookpository bookpository;


    public Booktroller(Bookpository bookpository) {
        this.bookpository = bookpository;
    }

    @GetMapping("/books")
    public String books(Model T){
        List<Book> booksss = bookpository.findAll();

        T.addAttribute("itsEmpty", booksss.size()==0);
        T.addAttribute("manyBooks", booksss);

        return "books/books";
    }

    @GetMapping("/books/input")
    public String bookForm (Model A){
        A.addAttribute("book", new Book());
        return "books/more";
    }

    @PostMapping("/books/input")
    public String inputBook (@ModelAttribute Book clickToAddBook){
        bookpository.save(clickToAddBook);
        return "redirect:/books";
    }

    @GetMapping("/books/{id}")
    public String showBook(@PathVariable long id, Model mod){
        Book book = bookpository.getOne(id);
        mod.addAttribute("BookId", id);
        mod.addAttribute("theBook", book);
        boolean isNull = false;
        try{
            book.getTitle();
        } catch (Exception bad){
//            System.out.println(bad.getMessage() + " :: " +bad.getCause());
            isNull = true;
        }
        mod.addAttribute("isNull", isNull);

        return "books/thisbook";
    }

    @GetMapping("/books/{id}/edit-book")
    public String editForm(Model mod, @PathVariable long id){
        Book edit = bookpository.getOne(id);
        mod.addAttribute("editBook",edit);
        return "books/edit";
    }

    @PostMapping("/books/{id}/edit-book")
    public String edit(@PathVariable long id,
                       @RequestParam(name = "title") String title,
                       @RequestParam(name = "author") String author,
                       @RequestParam(name = "genre") String genre,
                       @RequestParam(name = "releaseDate") String releaseDate) {
        Book theBook = bookpository.getOne(id);
        theBook.setTitle(title);
        theBook.setAuthor(author);
        theBook.setGenre(genre);
        theBook.setReleaseDate(releaseDate);
        bookpository.save(theBook);
        return "redirect:/books/{id}";
    }

    @PostMapping("/books/{id}/delete-book")
    public String delet(@PathVariable long id){
        bookpository.deleteById(id);
        return "redirect:/books/{id}";
    }
}
