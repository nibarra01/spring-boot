package com.codebound.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class FormController {
    @GetMapping("/show-form")
    public String enterForm(){
        return "company-form";
    }

    @PostMapping("/process-form")
    public String processForm(Model model,
                           @RequestParam(name = "firstName") String first,
                           @RequestParam(name = "lastName") String last,
                           @RequestParam(name = "city") String c,
                           @RequestParam(name = "state") String s,
                           @RequestParam(name = "email") String e) {
        model.addAttribute("fn", first);
        model.addAttribute("ln", last);
        model.addAttribute("ci", c);
        model.addAttribute("st", s);
        model.addAttribute("em", e);

        return "show-form-submit";
    }
}
