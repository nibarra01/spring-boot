package com.codebound.springnotes.controllers;

import com.codebound.springnotes.Models.Employee;
import com.codebound.springnotes.repositories.EmployeeAppRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Controller
public class EmployeeDirectoryAppController {
    private final EmployeeAppRepository rep;
    private Employee undoCache;
    private int next_employee_number;

    public EmployeeDirectoryAppController(EmployeeAppRepository rep) {
        this.rep = rep;
    }

    @GetMapping("/employee-app")
    public String index(Model index){
        List<Employee> EL = rep.findAll();

        index.addAttribute("empty", EL.size()==0);
        index.addAttribute("employeeList", EL);

        return "EmployeeApp/index";
    }

//    -------------------------------------------

    @GetMapping("/employee-app/add")
    public String addGet(Model add){
       this.next_employee_number = findEID();
       Employee emp = new Employee();
       emp.setEmployee_number(this.next_employee_number);
       add.addAttribute("emp", emp);
       add.addAttribute("default", this.next_employee_number);
       return "EmployeeApp/new";
    }

    @PostMapping("/employee-app/add")
    public String addPost(@ModelAttribute Employee newEmp){
        rep.save(newEmp);
        return "redirect:/employee-app";
    }

    //    -------------------------------------------

    @GetMapping("/employee-app/edit/{id}")
    public String editGet(Model edit, @PathVariable int id){
        Employee editEmp = rep.getOne(id);
        this.undoCache = rep.getOne(id);
        String name = editEmp.getFirst_name() + " " + editEmp.getLast_name();
        edit.addAttribute("editEmployee", editEmp);
        edit.addAttribute("name",name);
        return "EmployeeApp/edit";
    }

    @PostMapping("/employee-app/edit/{id}")
    public String editPost(@PathVariable int id,
                          @RequestParam(name = "en") String en,
                          @RequestParam(name = "fn") String fn,
                          @RequestParam(name = "ln") String ln,
                          @RequestParam(name = "em") String em){
//        this.undoCache = rep.getOne(id);
        Employee editEmp = rep.getOne(id);
//        System.out.println(en);



        editEmp.setEmployee_number(Integer.parseInt(en));
        editEmp.setFirst_name(fn);
        editEmp.setLast_name(ln);
        editEmp.setEmail(em);
        rep.save(editEmp);
        return "redirect:/employee-app/edit/confirm/{id}";
    }

    //    -------------------------------------------

    @PostMapping("/employee-app/delete/{id}")
    public String delet(@PathVariable int id){
        this.undoCache = rep.getOne(id);
        rep.deleteById(id);
        return "redirect:/employee-app/delete/confirm/{id}";
    }

    //    -------------------------------------------

    @GetMapping("/employee-app/delete/confirm/{id}")
    public String confirmDeleteGet(@PathVariable int id, Model conf){
        conf.addAttribute("ID", id);
        String nameOld = this.undoCache.getFirst_name() + " " + this.undoCache.getLast_name();
        conf.addAttribute("enOld",this.undoCache.getEmployee_number());
        conf.addAttribute("fnOld",this.undoCache.getFirst_name());
        conf.addAttribute("lnOld",this.undoCache.getLast_name());
        conf.addAttribute("emOld",this.undoCache.getEmail());
        conf.addAttribute("nameOld", nameOld);

        return "EmployeeApp/confirm1";
    }

    @GetMapping("/employee-app/edit/confirm/{id}")
    public String confirmEditGet(@PathVariable int id, Model conf){
        Employee eddel = rep.getOne(id);
        conf.addAttribute("id", id);
        conf.addAttribute("confirmChanges", eddel);
        conf.addAttribute("enOld",this.undoCache.getEmployee_number());
        conf.addAttribute("fnOld",this.undoCache.getFirst_name());
        conf.addAttribute("lnOld",this.undoCache.getLast_name());
        conf.addAttribute("emOld",this.undoCache.getEmail());

            String name = eddel.getFirst_name() + " " + eddel.getLast_name();
            conf.addAttribute("en",eddel.getEmployee_number());
            conf.addAttribute("fn",eddel.getFirst_name());
            conf.addAttribute("ln",eddel.getLast_name());
            conf.addAttribute("em",eddel.getEmail());
            conf.addAttribute("name", name);

        return "EmployeeApp/confirm2";
    }

    @GetMapping("/employee-app/confirm/restore")
    public String no(){
        rep.save(undoCache);
        this.undoCache = null;
        return "redirect:/employee-app";
    }

    @GetMapping("employee-app/confirm/revert/{id}")
    public String undo(@PathVariable int id){
        rep.deleteById(id);
        rep.save(this.undoCache);
        this.undoCache = null;
        return "redirect:/employee-app";
    }

    @GetMapping("/employee-app/confirm/confirmed")
    public String yes(){
        this.undoCache = null;
        return "redirect:/employee-app";
    }

    //    -------------------------------------------

    private int findEID(){
        int newEID = 0;
        for (Employee x : rep.findAll()){
//            System.out.println(newEID);
//            System.out.println(newEID < x.getEmployee_number());
            if(newEID < x.getEmployee_number()){
                newEID = x.getEmployee_number();
            }
//            System.out.println(newEID < x.getEmployee_number());
//            System.out.println(newEID);
//            System.out.println(x.getEmployee_number());
        }
        newEID++;
        return newEID;
    }
}
