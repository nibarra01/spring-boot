package com.codebound.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DiceController {

    public static int rollDie(int roll){
        return (int)(Math.random()*roll) + 1;
    }

    @GetMapping("/casino-roll")
    public String casinoA(){
        return "casino-roll";
    }



    @GetMapping("/casino-roll/{num}")
    public String casinoB(@PathVariable String num, Model model){
        model.addAttribute("choice", num);
        int number = rollDie(6);
        model.addAttribute("roll", number);

        if (Integer.parseInt(num) == number){
            return "casino-roll-win";
        } else {
            return "casino-roll-lose";
        }
    }
}
