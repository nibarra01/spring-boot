package com.codebound.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MathController {
    @GetMapping("/add/{a}/and/{b}")
    @ResponseBody
    public int addition(@PathVariable int a, @PathVariable int b){
        return a + b;
    }
    @GetMapping("/subtract/{a}/and/{b}")
    @ResponseBody
    public int subtraction(@PathVariable int a, @PathVariable int b){
        return a - b;
    }
    @GetMapping("/multiply/{a}/and/{b}")
    @ResponseBody
    public int multiplication(@PathVariable int a, @PathVariable int b){
        return a * b;
    }
    @GetMapping("/divide/{a}/and/{b}")
    @ResponseBody
    public String division(@PathVariable int a, @PathVariable int b){
        return "Product: " + a/b + "\nRemainder: " + a%b;
    }
}
