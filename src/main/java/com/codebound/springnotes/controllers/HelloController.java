package com.codebound.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {
    @GetMapping("/hello/{user}")
    @ResponseBody
    public String helloController(@PathVariable String user){
        return "Hello Timmy!" +
                "\n the variable for {user} is " + user;
    }
}
