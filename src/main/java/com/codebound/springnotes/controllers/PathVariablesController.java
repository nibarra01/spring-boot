package com.codebound.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PathVariablesController {
    @GetMapping("/example/{name}")
    @ResponseBody
    public String greeting(@PathVariable String name){
        return "Hello " + name + "!";
    }
//    Spring allows us to use path variables. That is variables that are part of the URL request, as opposed
//    to being passed as a query string or as part of the request body
}
