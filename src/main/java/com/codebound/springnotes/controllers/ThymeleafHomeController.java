package com.codebound.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ThymeleafHomeController {
    @GetMapping("/thymeleaf-home")
    public String welcome(){
        return "thymeleaf-view";
//        thymeleaf-view is the name of our view
    }
}
