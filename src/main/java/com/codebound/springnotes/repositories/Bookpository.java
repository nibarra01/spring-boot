package com.codebound.springnotes.repositories;

import com.codebound.springnotes.Models.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Bookpository extends JpaRepository<Book, Long> {
}
