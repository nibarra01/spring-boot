package com.codebound.springnotes.repositories;

import com.codebound.springnotes.Models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeAppRepository extends JpaRepository<Employee, Integer> {
}
