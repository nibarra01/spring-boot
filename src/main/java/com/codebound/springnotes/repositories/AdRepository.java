package com.codebound.springnotes.repositories;
/*
Repossitories
- Similar to DAOs
- They are an abstraction of interacint with a database
- usually implemetns in Java using DAOs to a predefined parent class named JpaRepository
 */

import com.codebound.springnotes.Models.Ad;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdRepository extends JpaRepository<Ad, Long> {
}
// just by defining an interface that extends JpaRepository we can start using this interface in other classes
//allows us to inject a dependency in our controller
